﻿using System;

namespace ConsoleTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public string GetHelloWorld()
        {
            return "Hello World!";
        }
    }
}
